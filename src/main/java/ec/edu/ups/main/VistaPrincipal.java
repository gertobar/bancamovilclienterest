package ec.edu.ups.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.namespace.QName;

import ec.edu.ups.gestion.GestionTransaccion;
import ec.edu.ups.utils.Respuesta;
import ec.edu.ups.utils.Trans;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField txtOrigen;
	private JTextField txtDestino;
	private JTextField txtMonto;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaPrincipal frame = new VistaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		 

	      
	      
	}


	/**
	 * Create the frame.
	 */
	public VistaPrincipal() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 382);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();

		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Transacciones");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewLabel.setBounds(126, 11, 193, 31);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Cuenta Origen");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel_1.setBounds(23, 111, 111, 29);
		panel.add(lblNewLabel_1);
		
		final JLabel lblNewLabel_2 = new JLabel("Cuenta Destino");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel_2.setBounds(23, 164, 130, 29);
		panel.add(lblNewLabel_2);
		lblNewLabel_2.setVisible(false);
		
		JLabel lblNewLabel_3 = new JLabel("Monto");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel_3.setBounds(57, 223, 64, 29);
		panel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Tipo Transaccion");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel_4.setBounds(23, 53, 130, 29);
		panel.add(lblNewLabel_4);
		
		final JComboBox cbxTipo = new JComboBox();
		cbxTipo.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		cbxTipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String v1 = (String) cbxTipo.getSelectedItem().toString();
				//String text = txtMonto.getText(); 
				if(v1 == "tranferencia") {
					txtDestino.setVisible(true);
					lblNewLabel_2.setVisible(true);
	
				}else if(v1 == "Deposito") {
					txtDestino.setVisible(false);
					lblNewLabel_2.setVisible(false);
					
				}else if(v1 == "Retiro") {
					txtDestino.setVisible(false);
					lblNewLabel_2.setVisible(false);
					
				}
			}
		});
		cbxTipo.setBounds(173, 53, 111, 32);
		cbxTipo.addItem("Selecione");
		cbxTipo.addItem("tranferencia");
		cbxTipo.addItem("Deposito");
		cbxTipo.addItem("Retiro");
		panel.add(cbxTipo);
		
		txtOrigen = new JTextField();
		txtOrigen.setBounds(173, 111, 111, 32);
		panel.add(txtOrigen);
		txtOrigen.setColumns(10);
		
		txtDestino = new JTextField();
		txtDestino.setBounds(173, 164, 111, 32);
		panel.add(txtDestino);
		txtDestino.setColumns(10);
		txtDestino.setVisible(false);
		
		txtMonto = new JTextField();
		txtMonto.setBounds(173, 223, 108, 32);
		panel.add(txtMonto);
		txtMonto.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				Trans t= new Trans();
				Respuesta r  =new Respuesta();
				GestionTransaccion gt = new GestionTransaccion();
				
				String v1 = (String) cbxTipo.getSelectedItem().toString();
				String text = txtMonto.getText(); 
				double value = Double.parseDouble(text);
				
				
				
				if(v1 == "tranferencia") {
					try {
						txtDestino.setVisible(true);
					t.setCuentaorigen(txtOrigen.getText());
					t.setCuentadestino(txtDestino.getText());
					t.setMonto(value);	
					t.setTipo(v1);
				Respuesta _transacciones__return = gt.realizarTransaccion(t);
				System.out.println("transacciones.result=" + _transacciones__return.toString());
				JOptionPane.showMessageDialog(null, _transacciones__return.toString());
					}catch (Exception ex){
						r.setMensaje(ex.getMessage());
						
						JOptionPane.showMessageDialog(null, "Transaccion Realizada");
						
					}
				
				
				}else if(v1 == "Deposito") {
					t.setCuentaorigen(txtOrigen.getText());
					t.setCuentadestino("");
					t.setMonto(value);
					t.setTipo(v1);
					Respuesta _transacciones__return = gt.realizarTransaccion(t);
					System.out.println("transacciones.result=" + _transacciones__return.toString());
					JOptionPane.showMessageDialog(null, _transacciones__return.toString());
					
				}else if(v1 == "Retiro") {

					t.setCuentaorigen(txtOrigen.getText());
					t.setCuentadestino("");
					t.setMonto(value);
					t.setTipo(v1);
					Respuesta _transacciones__return = gt.realizarTransaccion(t);
					System.out.println("transacciones.result=" + _transacciones__return.toString());
					JOptionPane.showMessageDialog(null, _transacciones__return.toString());
					
				}
				
			}
		});
		btnAceptar.setBounds(78, 283, 117, 39);
		panel.add(btnAceptar);
		
		JLabel lblNewLabel_5 = new JLabel("$");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewLabel_5.setForeground(Color.GREEN);
		lblNewLabel_5.setBounds(291, 223, 25, 29);
		panel.add(lblNewLabel_5);
	}
	

}
