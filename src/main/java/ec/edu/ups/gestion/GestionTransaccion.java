package ec.edu.ups.gestion;

import ec.edu.ups.utils.Respuesta;
import ec.edu.ups.utils.Trans;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

public class GestionTransaccion {
	
	private String WS_SAVE_TRANSACCION = "http://localhost:8080/proyectoappdisserver/ws/cuenta";
	//"http://localhost:8080/proyectoappdisserver/ws/cuenta/transaccion";

	public Respuesta realizarTransaccion(Trans t) {
		
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_SAVE_TRANSACCION);
		Respuesta respuesta = target.request().post(Entity.json(t), Respuesta.class);
			
		return respuesta;
	}

}
