package ec.edu.ups.EN;


import java.io.Serializable;
import java.util.Date;


import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaccion implements Serializable{
	
	private int id;
	private Date fecha;
	private Double monto;
	
	private String tipoTransaccion;

	private CuentaAhorro cuenta;

	@XmlTransient
	private String cuent;

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getTipoTransaccion() {
		return tipoTransaccion;
	}
	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public CuentaAhorro getCuenta() {
		return cuenta;
	}
	public void setCuenta(CuentaAhorro cuenta) {
		this.cuenta = cuenta;
	}

	public String getCuent() {
		return cuent;
	}
	public void setCuent(String cuent) {
		this.cuent = cuent;
	}

	

}
