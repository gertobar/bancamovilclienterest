package ec.edu.ups.EN;


import java.io.Serializable;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CuentaAhorro implements Serializable{
	

	private String numeroCuenta;
	private Double saldoCuenta;
	

    private Usuario usuario;

    private List<Transaccion> listaTra;
	

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public Double getSaldoCuenta() {
		return saldoCuenta;
	}

	public void setSaldoCuenta(Double saldoCuenta) {
		this.saldoCuenta = saldoCuenta;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Transaccion> getListaTra() {
		return listaTra;
	}

	public void setListaTra(List<Transaccion> listaTra) {
		this.listaTra = listaTra;
	}
	public void addTransaccion(Transaccion t) {
		if(listaTra==null) {
			listaTra= new ArrayList<Transaccion>();
	}
		this.listaTra.add(t);
	}
}
