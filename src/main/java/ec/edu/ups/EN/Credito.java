package ec.edu.ups.EN;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;


public class Credito implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String tipo;
	private Double valor;
	private Double saldo;
	private Date fechaVence;
	private String estado;

    private List<Transaccion> listaTransacciones;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Date getFechaVence() {
		return fechaVence;
	}
	public void setFechaVence(Date fechaVence) {
		this.fechaVence = fechaVence;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	

}
