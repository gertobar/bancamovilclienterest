package ec.edu.ups.EN;


import java.util.ArrayList;
import java.util.List;


public class Empleado {


	private String cedula;
	private String nombre;
	private String apellido;
	private String direccion;
	private String correo;
	private String cargo;
	private String password;
	

    private List<Transaccion> listaTransaciones;
	
	

    private List<Sesion> listaSesiones;
	
	
	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Transaccion> getListaTransaciones() {
		return listaTransaciones;
	}

	public void setListaTransaciones(List<Transaccion> listaTransaciones) {
		this.listaTransaciones = listaTransaciones;
	}
	
	
	public List<Sesion> getListaSesiones() {
		return listaSesiones;
	}

	public void setListaSesiones(List<Sesion> listaSesiones) {
		this.listaSesiones = listaSesiones;
	}

	@Override
	public String toString() {
		return "Empleado [nombre=" + nombre + ", apellido=" + apellido + ", correo=" + correo + ", password=" + password
				+ "]";
	}

	public void addSesiones(Sesion e) {
		if(this.listaSesiones==null)
			this.listaSesiones = new ArrayList<Sesion>();
		this.listaSesiones.add(e);
	}
	

}
