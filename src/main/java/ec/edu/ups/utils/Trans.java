package ec.edu.ups.utils;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.edu.ups.EN.CuentaAhorro;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Trans implements Serializable {
	
	private String cuentaorigen;
	private String cuentadestino;
	private String tipo;
	
	private Double monto;

	
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCuentaorigen() {
		return cuentaorigen;
	}

	public void setCuentaorigen(String cuentaorigen) {
		this.cuentaorigen = cuentaorigen;
	}

	public String getCuentadestino() {
		return cuentadestino;
	}

	public void setCuentadestino(String cuentadestino) {
		this.cuentadestino = cuentadestino;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}
	
	
	
}
